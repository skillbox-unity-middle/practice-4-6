using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TMPro;
using UnityEngine;
using UnityGoogleDrive;

public class Button : MonoBehaviour
{
    [SerializeField] private TMP_InputField _nicknameInputField;
    [SerializeField] private TMP_InputField _classInputField;
    [SerializeField] private TMP_InputField _characterInputField;

    private string _fileId;
    private bool _isFileIdExists;
    private PlayerInfo _playerInfo;

    private void Start()
    {
        _playerInfo = new PlayerInfo();
        string fileId = PlayerPrefs.GetString("fileId");
        Debug.Log(fileId);
        if (fileId.Equals(string.Empty))
        {
            _isFileIdExists = false;
        }
        else
        {
            _fileId = fileId;
            _isFileIdExists = true;
        }
    }

    public void UploadButtonPressed()
    {
        StartCoroutine("CreateFile");
    }

    public void DownloadButtonPressed()
    {
        StartCoroutine("DownloadFile");
    }

    private IEnumerator CreateFile()
    {
        StartCoroutine("TryGetFile");
        if (_isFileIdExists)
        {
            var file = CreateFileForGoogle();
            var request = GoogleDriveFiles.Update(_fileId, file);
            yield return request.Send();
            print(request.IsError);
        }
        else
        {
            var file = CreateFileForGoogle();
            var request = GoogleDriveFiles.Create(file);
            request.Fields = new List<string> { "id" };
            yield return request.Send();
            print(request.IsError);
            _fileId = request.ResponseData.Id;
            PlayerPrefs.SetString("fileId", _fileId);
            _isFileIdExists = true;
            PlayerPrefs.Save();
        }
    }

    private IEnumerator DownloadFile()
    {
        if (_isFileIdExists)
        {
            StartCoroutine("TryGetFile");
            var request = GoogleDriveFiles.Download(_fileId);
            yield return request.Send();
            print(request.IsError);
            var stream = new StreamReader(new MemoryStream(request.ResponseData.Content));
            string str = stream.ReadLine();
            PlayerInfo playerInfo = JsonUtility.FromJson<PlayerInfo>(str);
            _nicknameInputField.text = playerInfo.nickname;
            _classInputField.text = playerInfo.className;
            _characterInputField.text = playerInfo.character;
            stream.Close();
        }
        else
        {
            Debug.LogWarning("_isFileExists false");
        }
    }

    private IEnumerator TryGetFile()
    {
        var getRequest = GoogleDriveFiles.Get(_fileId);
        yield return getRequest.Send();
        if (getRequest.IsError)
        {
            _fileId = string.Empty;
            PlayerPrefs.SetString("fileId", _fileId);
            _isFileIdExists = false;
            PlayerPrefs.Save();
        }
    }

    private UnityGoogleDrive.Data.File CreateFileForGoogle()
    {
        _playerInfo.nickname = _nicknameInputField.text;
        _playerInfo.className = _classInputField.text;
        _playerInfo.character = _characterInputField.text;
        var json = JsonUtility.ToJson(_playerInfo);

        return new UnityGoogleDrive.Data.File { Name = "GameData.json", Content = Encoding.ASCII.GetBytes(json) };
    }
}
