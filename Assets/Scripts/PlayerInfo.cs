﻿using System;

[Serializable]
public class PlayerInfo
{
    public string nickname;
    public string className;
    public string character;
}